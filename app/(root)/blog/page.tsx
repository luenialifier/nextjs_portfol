import Container from "@/components/ui/container";
import Title from "@/components/ui/title";
import ListBlog from "../sections/list-blog";

const BlogPage = () => {
    return ( 
        <main>
            <Container>
                <Title  title="Blog" className=""/>
                <ListBlog />

            </Container>
        </main>
     );
}
 
export default BlogPage;