import Badge from "@/components/ui/badge";
import Container from "@/components/ui/container";
import Title from "@/components/ui/title";
import Image from "next/image";


const BlogDetails = () => {
    return ( 
        
        <div className="w-[800px]">
            <Badge label="Design" className="text-primary"/>
            <Title title="¿Cuál es el verdadero potencial de la IA en el diseño? " className="mt-0 mb-10"/>
            <div className="relative w-full h-96 rounded-xl">
                <Image src="https://israelnoticias.com/wp-content/uploads/2022/06/The-RAH-66-Comanche-Stealth-Helicopter-il.webp" alt="blogImage" fill className=""/>

            </div>
        </div>
     ); 
}
 
export default BlogDetails;