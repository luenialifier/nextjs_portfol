
'use client';
import { ReactNode, useState } from "react";
import Sidebar from '@/components/shared/sidebar'
import { HiMenu, HiOutlineMenu } from "react-icons/hi"
import ButtonIcon from "@/components/ui/button-icon";
import Footer from "@/components/shared/footer";

const MainLayout = ({ children }: { children: ReactNode }) => {
    const [showMenu, setShowMenu] = useState(false);
    return (
        <div>
            <Sidebar showMenu={showMenu} onClose={() => setShowMenu(false) } />
            <ButtonIcon icon={HiOutlineMenu} onClick={()=> setShowMenu(true)} className="lg:hidden fixed right-4 bottom-4 z-30 bg-[#FFC480] p-4 rounded-full" />
            
                
            
            {children}
            <Footer />
        </div>
    ); 

}
export default MainLayout;