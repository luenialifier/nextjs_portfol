import { information } from '@/assets/data';
import ButtonSocialMedia from '@/components/ui/button-social-media';
import Image from 'next/image'
import { FaInstagram , FaXTwitter, FaLinkedin , FaYoutube} from "react-icons/fa6";
import HeroSection from './sections/hero-section';
import FeatureWorkSection from './sections/feature-work-section';
import TesticulSection from './sections/testicul-section';
import VentureSection from './sections/venture-section';
import BlogSection from './sections/blog-sections';

export default function Home() {
  
  return (
   <>
   <HeroSection />
   
   <FeatureWorkSection />

   <TesticulSection />

   <VentureSection />

   <BlogSection />
   </>
    );
  
  
}
