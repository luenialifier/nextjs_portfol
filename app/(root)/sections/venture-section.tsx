import CardVentures from "@/components/ui/card-ventures";
import Container from "@/components/ui/container";
import Title from "@/components/ui/title";
import ListVentures from "./list-ventures";

const VentureSection = () => {
    return (  <section id='ventures' className="border-b border-[#DB9881]/30 py-10 px-7">
        <Container>
            <ListVentures className={""} />
        </Container>
    </section>);
}
 
export default VentureSection;





