import { information } from '@/assets/data';
import ButtonSocialMedia from '@/components/ui/button-social-media';
import Image from 'next/image'

import Container from '@/components/ui/container';
const HeroSection = () => {
    return (
        <section id='home' className='border-b border-[#DB9881]/30 py-10 px-7'>
            <Container>
                <div>
                    <div className='space-y-10'>
                        <div className='relative w-24 h-24'>
                            <Image src={information.photo} alt="logo" fill className='object-cover rounded-xl ' />

                        </div>
                        <div className='max-w-3xl'>
                            <h1 className='text-4xl  text-white mb-5'>Qué Tal, — soy el Chemo del Design & Zero Code Bro</h1>
                            <p>Soy un diseñador atrevido con 20 años de experiencia especializado en varios campos del diseño gráfico,impresión digital a gran formato -stands - web - nube</p>
                        </div>
                        <div className=' flex item-center gap-4'>
                            {information.socialMedia.map((social) => <ButtonSocialMedia key={social.href} href={social.href} icon={social.icon} label={''} />)}
                        </div>
                    </div>
                </div>
            </Container>
        </section>
    );

}
export default HeroSection ;