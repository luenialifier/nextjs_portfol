import CardBlog from "@/components/ui/card-blog";

const ListBlog = () => {
    return ( 
        <div className="space-y-12">
        <CardBlog image="https://img.freepik.com/vector-gratis/insignia-que-fluye-aplicacion_74855-3835.jpg?w=740&t=st=1707163515~exp=1707164115~hmac=b834d0414abb53eb5a2df6703720556f6ce4a8463ee4ca1c233138e82b6f6267"
        href="/blog/details" title=" Difference between UX and UI" category="Design" ago="4 mins" description="Totaly Exeptionaly"/>

    <CardBlog image="https://img.freepik.com/vector-gratis/concepto-fondo-formas-superpuestas_23-2148669866.jpg?t=st=1709136929~exp=1709140529~hmac=d73dfcfc4737734574ac5aac4ce8e51e9cb14b20a284756c6342157372943584&w=1380"
        href="/blog/details" title=" Difference between UX and UI" category="Design" ago="4 mins" description="Totaly Exeptionaly"/>
    

    <CardBlog image="https://img.freepik.com/foto-gratis/render-3d-fondo-comunicaciones-red-lineas-puntos-conexion_1048-14022.jpg?t=st=1709137324~exp=1709140924~hmac=c528168bec6685c974470d14dbe004de07b5bc25e88633e3b601d5b5dafac044&w=996"
        href="/blog/details" title=" Difference between UX and UI" category="Design" ago="4 mins" description="Totaly Exeptionaly"/>

<CardBlog image="https://img.freepik.com/foto-gratis/fondo-texturizado-hexagonal-redes_23-2150080742.jpg?t=st=1709137409~exp=1709141009~hmac=83fccd38a34e0822500eb4119e373f4de8e848e8652f74722283de4d6606bc67&w=1060"
        href="/blog/details" title=" Difference between UX and UI" category="Design" ago="4 mins" description="Totaly Exeptionaly"/>

<CardBlog image="https://img.freepik.com/vector-gratis/fondo-abstracto-particulas-brillantes_23-2148309414.jpg?t=st=1709136865~exp=1709140465~hmac=7ed3ee40edebe71a56b6d35233357feed1e0fbf6dcb0485b2944330fa5fea916&w=1380"
        href="/blog/details" title=" Difference between UX and UI" category="Design" ago="4 mins" description="Totaly Exeptionaly"/>
        </div> 
     );
}
 
export default ListBlog;