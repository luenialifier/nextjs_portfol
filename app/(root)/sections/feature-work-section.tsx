
'use client';

import Title from "@/components/ui/title";
import Container from '@/components/ui/container';
import CardFeaturedWork from "@/components/ui/card-featured-work";
import Button from "@/components/ui/button";
import { useRouter } from "next/navigation";
import ListFeaturedWork from "@/components/list-featured-work";



const FeatureWorkSection = () => {
    const router = useRouter();

    return (  
        <section id='featureWork' className="py-7 border-b border-[#DB9881]/30 py-10 px-7">
            <Container>
                <Title title='Feature Work' className="" />
                <ListFeaturedWork />
                <Button  label="View All Chambas" onClick={() => router.push('/work')} />
            </Container>

        </section>
    );
}
 
export default FeatureWorkSection; 