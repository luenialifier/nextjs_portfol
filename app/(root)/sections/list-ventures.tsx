
import CardVentures from "@/components/ui/card-ventures";
import { cn } from "@/libs/utils";

interface ListVenturesProps {
    className: string;
}

const ListVentures = ({className}: ListVenturesProps) => {
    return ( 
        <div className={cn('grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10s', className)}>
        <CardVentures href='https://kevinhagat.lemonssqueezy.com/checkout/buy/c7022897-3823-482-c-a39b-3578443f50cd'
        image='https://img.freepik.com/vector-gratis/logotipo-degradado_23-2148149233.jpg?w=740&t=st=1706719375~exp=1706719975~hmac=20d7d4433275923fb787ef705671004bd09a4083d9a7b27e7de6f55407ad0570' 
        title='Framer Templates' description='Creating Framer Templates for designers, agencies and content creators'
        />

        <CardVentures href='https://kevinhagat.lemonssqueezy.com/checkout/buy/c7022897-3823-482-c-a39b-3578443f50cd'
        image='https://img.freepik.com/vector-gratis/plantilla-logo-abstracto-marca_1043-174.jpg?w=740&t=st=1706910960~exp=1706911560~hmac=a901f56b5b76dd60f1fdc917df2797bd0f6d11e2b2c2c84d9074de75118ba8c2' 
        title='Curated to create' description='Creating Framer Templates for designers, agencies and content creators'
        />

<CardVentures href='https://kevinhagat.lemonssqueezy.com/checkout/buy/c7022897-3823-482-c-a39b-3578443f50cd'
        image='https://img.freepik.com/vector-premium/diseno-logotipo-agencia-minima-dos-colores-fondo-blanco-vector-premium_729473-43.jpg?w=740' 
        title='Moinimal Visuals' description='Creating Framer Templates for designers, agencies and content creators'
        />
    </div> 
     );
}
 
export default ListVentures;