import CardTesticul from '@/components/ui/card-testicul';
import Container from '@/components/ui/container';
import Title from '@/components/ui/title';

const TesticulSection = () => {
    return ( 
        <section id='testicul' className="py-7 border-b border-[#DB9881]/30 py-10 px-7">
            <Container>
                <Title  title='Testimonials'/>
                <div className='grid grid-cols-1 md:grid-cols-2 gap-10'>
                    <CardTesticul message='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in aliquam nunc. Morbi hendrerit pellentesque accumsan. Pellentesque volutpat, ante vulputate iaculis commodo, lorem metus consectetur quam, vitae vestibulum sem nisi id quam. Vestibulum accumsan varius nulla. Curabitur hendrerit sem quis lectus rhoncus vulputate. Aliquam semper ex ex, mattis auctor mauris viverra id. Donec ut sodales quam. Sed ultrices tortor a.'
image='https://img.freepik.com/foto-gratis/retrato-fotografico-stock-adorable-nino-pequeno-gafas-sol-negras-moda-haciendo-pucheros-sus-labios-graciosamente-sobre-fondo-amarillo-aislar_132075-13010.jpg?w=1380&t=st=1702310378~exp=1702310978~hmac=3bc29e56a4abb41effcf768d0e657b9ed0f317ccfcf1b9848dd5bb8d53ded143' name='Carol Phillie' position='tester Jr.'  />
                    
                    <CardTesticul message='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in aliquam nunc. Morbi hendrerit pellentesque accumsan. Pellentesque volutpat, ante vulputate iaculis commodo, lorem metus consectetur quam, vitae vestibulum sem nisi id quam. Vestibulum accumsan varius nulla. Curabitur hendrerit sem quis lectus rhoncus vulputate. Aliquam semper ex ex, mattis auctor mauris viverra id. Donec ut sodales quam. Sed ultrices tortor a.'
image='https://img.freepik.com/foto-gratis/nina-sonriente-que-muestra-lengua_171337-3419.jpg?w=740&t=st=1702319957~exp=1702320557~hmac=523e8cfc216133368f1db1d5a0031f257c40b3a343057150140600266394fd9c' name='Lylie Opson' position='developer Jr.'  />

                    <CardTesticul message='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in aliquam nunc. Morbi hendrerit pellentesque accumsan. Pellentesque volutpat, ante vulputate iaculis commodo, lorem metus consectetur quam, vitae vestibulum sem nisi id quam. Vestibulum accumsan varius nulla. Curabitur hendrerit sem quis lectus rhoncus vulputate. Aliquam semper ex ex, mattis auctor mauris viverra id. Donec ut sodales quam. Sed ultrices tortor a.'
image='https://img.freepik.com/foto-gratis/nino-posando-perforadora_23-2148475818.jpg?w=826&t=st=1702320116~exp=1702320716~hmac=e968adf8ff5df330dc30401d8ca394839e84bada2c8f78a74b1debff06e20826' name='Osvald Terper' position='inventor Jr.' />
                    
                    <CardTesticul message='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in aliquam nunc. Morbi hendrerit pellentesque accumsan. Pellentesque volutpat, ante vulputate iaculis commodo, lorem metus consectetur quam, vitae vestibulum sem nisi id quam. Vestibulum accumsan varius nulla. Curabitur hendrerit sem quis lectus rhoncus vulputate. Aliquam semper ex ex, mattis auctor mauris viverra id. Donec ut sodales quam. Sed ultrices tortor a.'
image='https://img.freepik.com/foto-gratis/retrato-nino-pequeno-lindo-feliz-anteojos_171337-7222.jpg?w=1380&t=st=1702322982~exp=1702323582~hmac=7dda4ae5bb7c355307b96cfe8777c6750a918f38bdc07603a771528fb3f205c0' name='Paul Alan' position='invester Sr.' />
                </div>
            </Container>
        </section>
     );
}
 
export default TesticulSection;