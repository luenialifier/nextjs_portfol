'use client';

import Button from "@/components/ui/button";
import CardBlog from "@/components/ui/card-blog";
import Container from "@/components/ui/container";
import Title from "@/components/ui/title";
import { useRouter } from "next/navigation";
import ListBlog from "./list-blog";


const BlogSection = () => {

    const router = useRouter();
    return ( 
        <section id='blog' className="border-b border-[#DB9881]/30 py-10 px-7">
            <Container>
                <Title title="Blog" className=""/>
                <ListBlog />
               
                    <Button  label="View All Blogs" onClick={( ) => router.push('/blog')} />
            </Container>
            
        </section>
     );
}
 
export default BlogSection;