'use client';

import Button from "@/components/ui/button";
import Title from "@/components/ui/title";
import Image from "next/image";
import Container from "@/components/ui/container";

const SectionHeader = () => {
    return ( 
        <section id='sectionHeader' className="border-b border-[#DB9881]/30 py-10 px-7">
            <Container>
        <Title title='Muy pensada Aplicación movil' className="mb-5 "/>
        <p className="text-[#81DBC6] text-2xl"> El objetivo fue presentar la marca corporativa en diferentes formatos y medios </p>
        <div className="flex items-center justify-between">
            <Button type="button" label="View Live App"  className="w-auto" onClick={() => {}}/>
            <ul className="space-y-3 mb-10">
                <li className="relative before:absolute before:w-2 before:h-2 before:border before:border-primary  before:-left-6 before:top-1/2 before:-translate-y-1/2">Industria: Blogin</li>
                <li className="relative before:absolute before:w-2 before:h-2 before:border before:border-primary  before:-left-6 before:top-1/2 before:-translate-y-1/2">Líneas de Tiempo: 2 semanas</li>
                <li className="relative before:absolute before:w-2 before:h-2 before:border before:border-primary  before:-left-6 before:top-1/2 before:-translate-y-1/2">Diseño de Producto</li>
            </ul>
        </div>
        <div className="relative w-full h-96 sm:h-[500px] lg:h-[700px] rounded-xl">
            <Image src="https://img.freepik.com/foto-gratis/mujer-buscando-monedas-criptograficas-usando-su-telefono-inteligente-mientras-casa_23-2149240357.jpg?w=740&t=st=1701451891~exp=1701452491~hmac=73d07ddc6ab33947ef70cf8c05402c596ce3e572886a8246f4cb2a7b890e13a4"
                        title="Diseño en diversos formatos" alt="Image" fill className="object-cover rounded-xl "/>
        </div>
        
        </Container>
        </section >
     );
}
  
export default SectionHeader;