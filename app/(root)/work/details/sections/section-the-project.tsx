import Container from "@/components/ui/container";
import Subtitle from "@/components/ui/subtitle";
import Image from "next/image";

const SectionTheProject = () => {
    return (
        <section id='sectionMyRole' className="border-b border-[#DB9881]/30 py-10">
            <Container>
                <div className="flex flex-col sm:flex-row sm:items-start sm:justify-between mb-10">
                    <Subtitle subtitle="El Proyecto" clasName="my-0" />
                    <ul className="space-y-8 ">
                        <li className="max-w-2xl">
                            <h4 className="text 2-xl text-[#FFC4B0] font-medium mb-3">Descripción breve</h4>
                            <p className="text-sm text-[#DB9881] font-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Praesent nulla magna, tempor sed tellus ut, maximus semper nulla.
                                In sit amet est at lacus blandit elementum eu sit amet velit.
                                Maecenas nec placerat nulla. </p>

                        </li>
                        <li className="max-w-2xl">
                            <h4 className="text 2-xl text-[#FFC4B0] font-medium mb-3">Resultados no tan previsibles</h4>
                            <p className="text-sm text-[#DB9881] font-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Praesent nulla magna, tempor sed tellus ut, maximus semper nulla.
                                In sit amet est at lacus blandit elementum eu sit amet velit.
                                Maecenas nec placerat nulla. </p>

                        </li>
                    </ul>


                </div>
                <div className="relative w-full h-96 sm:h-[500px] lg:h-[700px] rounded-xl mb-10">
                    <Image src="https://img.freepik.com/vector-gratis/plantilla-folleto-diseno-poligonal_23-2147509209.jpg?w=740"
                        alt="Image" fill className="object-cover rounded-xl " />
                </div>
                <div className="flex items-center justify between gap-10">
                    <div className="relative w-full h-96 sm:h-[500px] lg:h-[700px] rounded-xl">
                        <Image src="https://img.freepik.com/vector-gratis/plantilla-diseno-folleto-elegante-onda-verde_1017-14369.jpg?w=996"
                            alt="Image" fill className="object-cover rounded-xl " />
                    </div>
                    <div className="relative w-full h-96 sm:h-[500px] lg:h-[700px] rounded-xl">
                        <Image src="https://img.freepik.com/vector-gratis/plantilla-diseno-folleto-elegante-onda-verde_1017-14369.jpg?w=996"
                            alt="Image" fill className="object-cover rounded-xl " />
                    </div>
                </div>
            </Container>
        </section>
    )

};

export default SectionTheProject;

