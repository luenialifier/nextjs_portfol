import Container from "@/components/ui/container";
import Subtitle from "@/components/ui/subtitle";
import Image from "next/image";

const SectionMyRole = () => {
    return (
        <section id='sectionMyRole' className="border-b border-[#DB9881]/30 py-10">
            <Container>
                <div className="flex flex-col sm:flex-row sm:items-start sm:justify-between mb-10">
                    <Subtitle subtitle="Mi Rol Aquí" clasName="my-0" />
                    <ul className="space-y-8 ">
                        <li className="max-w-2xl">
                            <h4 className="text 2-xl text-[#FFC4B0] font-medium mb-3">Responsabilidades</h4>
                            <p className="text-sm text-[#DB9881] font-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Praesent nulla magna, tempor sed tellus ut, maximus semper nulla. 
                            In sit amet est at lacus blandit elementum eu sit amet velit.  
                            Maecenas nec placerat nulla. </p>
                            
                        </li>
                        <li className="max-w-2xl">
                            <h4 className="text 2-xl text-[#FFC4B0] font-medium mb-3">Construcciones y Deconstrucciones</h4>
                            <p className="text-sm text-[#DB9881] font-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Praesent nulla magna, tempor sed tellus ut, maximus semper nulla. 
                            In sit amet est at lacus blandit elementum eu sit amet velit.  
                            Maecenas nec placerat nulla. </p>
                            
                        </li>
                    </ul>

                     
                </div>
                <div className="relative w-full h-96 sm:h-[500px] lg:h-[700px] rounded-xl">
            <Image src="https://img.freepik.com/vector-gratis/diseno-colorido-folleto_1055-370.jpg?w=740"
                        title="Diseño en diversos formatos" alt="Image" fill className="object-cover rounded-xl "/>
        </div>
            </Container>
        </section>
    )

};

export default SectionMyRole;

