import Container from "@/components/ui/container";
import Title from "@/components/ui/title";
import ListVentures from "../sections/list-ventures";

const VenturesPage = () => {
    return (
    <main className="border-b border border-[#DB9881]/30">
      <Container>
      <Title title='Ventures' className="" />
    </Container>
    <hr className="border-b border-[#DB9881]/30" />
   <Container className="py-10">
   <ListVentures className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-1 gap-10 "/>
   </Container>
    </main>
  )
}
export default VenturesPage;