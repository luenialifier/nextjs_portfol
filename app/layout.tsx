import type { Metadata } from 'next'
import { Raleway, Inter } from 'next/font/google'
import './globals.css'
import { ReactNode } from 'react'

import { cn } from '@/libs/utils'

const font  = Raleway({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'web-portfol',
  description: 'otra propuesta más de carpeta de trabajos',
}

export default function RootLayout({
  children,
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <body className={cn('bg-dark text-[#DB9881]', font.className)}>
      
      <main className='lg:pl-[13vw] xl:pl-[13vw] px-5 lg:px-0'>{children}</main>
      </body>
    </html>
  )
}
