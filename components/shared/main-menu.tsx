import Link from "next/link";
import{ cn }from "@/libs/utils";
import { HiOutlineHome, HiOutlineBriefcase, HiOutlineArchive,HiOutlineViewList,HiOutlineUser ,HiOutlineMail } from "react-icons/hi";
import { BiShapeSquare } from "react-icons/bi"
import { usePathname } from "next/navigation";

const MainMenu = () => {

    const routes = [{
          label: 'Home',
          icon: HiOutlineHome,  
          href: '/',
    },
    {
        label: 'Work',
        icon: BiShapeSquare,  
        href: '/work',
  },
  {
    label: 'Store',
    icon: HiOutlineArchive,  
    href: '/store',
},
{
    label: 'Ventures',
    icon: HiOutlineBriefcase,  
    href: '/ventures',
},
{
    label: 'Blog',
    icon: HiOutlineViewList,  
    href: '/blog',
},
{
    label: 'About',
    icon: HiOutlineUser ,  
    href: '/about',
},
{
    label: 'Contact',
    icon: HiOutlineMail,  
    href: '/contact',
}



];

const pathname = usePathname();
    return ( <ul>
        <li>
            {routes.map((route) => (   
                <Link key={route.href} 
                      href={route.href} className={cn('flex items-center gap-4 p-5 px-8 border-b border-[#DB9881]/30 hover:bg-[#DB9881]/10  hover:text-white transition-colors duration-300', pathname === route.href && 'text-white')}>
                <route.icon  size={15} />
                {route.label}
                
                </Link>))}
            
        </li>
    </ul>
    );
};

export default MainMenu;