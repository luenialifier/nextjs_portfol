'use client';

import Link from "next/link";
import MainMenu from "@/components/shared/main-menu";

import { cn } from "@/libs/utils";

interface SidebarProps { 
    showMenu: boolean;
    onClose: () => void;
}

const Sidebar = ({ showMenu, onClose}: SidebarProps) => { 
    
    return (
    <>
        <aside className={cn("fixed  top-0 lg:left-0 bg-dark w-[12vw] w-[70vw] md:w-[30vw] lg:w-[13vw] xl:w-[13vw] h-full border-r border-[#FFC4B0]/30 transition-all duration-300 ease-in-out z-50", showMenu ? "left-0" : "right-full")}>
            <section className=" p-8 border-b border-[#DB9881]/30 ">
                <Link href='/' className="text -xl text-[#81DBC6] hover:text-primary transition-colors duration-300">Vermejo</Link>
                <h3 className="text-sm tracking-widest">Web Developer</h3>
            </section>
            <section>
                <MainMenu />
            </section>
        </aside>
        <div onClick={onClose} className={cn("fixed bg-[#FFC4B0]/20 z-40 left-0 top-0 w-full h-full lg:hidden", showMenu? "block" : "hidden")} />
    </>
    );
}

export default Sidebar; 