

import { cn } from "@/libs/utils";


interface ButtonProps {
    type?: 'button' | 'submit';
    label: string;
    className?: string;
    onClick: () => void;
    
}



const Button = ({ type = 'button', label, className, onClick}: ButtonProps) => {
    return <button type={type} onClick={onClick}  className={cn('py-3 px-4 border border-[#DB9881]  text-[#DB9881] w-full rounded-lg hover:bg-[ ]/30 duration-300 font-medium', className )} >{label}</button>;
};
 
export default Button;