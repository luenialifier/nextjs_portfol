import { cn } from "@/libs/utils";

interface SubtitleProps {
    subtitle: string;
    clasName?: string
}

const Subtitle = ({ subtitle, clasName }: SubtitleProps) => {
    return (
        <h2 className={cn('text-2xl ml-6 font-light text-[#FFC4B0] my-10 relative before:absolute before:w-3 before:h-3 before:border before:border-primary  before:-left-6 before:top-1/2 before:-translate-y-1/2', clasName)}>{subtitle}</h2>
    );
};

export default Subtitle;
