import { cn } from "@/libs/utils";

interface BadgeProps{
    label: string;
    className?: string;
}


const Badge = ({label, className}: BadgeProps) => {
    return ( 
        <h5 className={cn("text-[#81DBC6] uppercase tracking-widest text-xs")}>{label}</h5>
     );
}
 
export default Badge;