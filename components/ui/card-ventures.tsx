import Image from "next/image";

interface CardVenturesProps {
    href: string;
    title: string;
    description: string;
    image: string;
}


const CardVentures = ({href,title,description,image}: CardVenturesProps) => {
    return ( <a href={href}target='_blank' className="block group">
        <div className="relative w-14 h-14 rounded-xl mb-2">
            <Image src={image} alt='Image'
            fill className="object-cover rounded-xl "/>
        </div>
        <h3 className="text-white text-2xl mb-2 group-hover:text-primary transition-colors duration-300">{title}</h3>
        <p className="text-[#FFC4B0]">{description}</p>
    </a>  );
}
 
export default CardVentures;