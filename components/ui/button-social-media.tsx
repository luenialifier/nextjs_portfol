
import { IconType } from "react-icons";
interface ButtonSocialProps{
    label: string;
    icon: IconType;
    href: string;

}

const ButtonSocialMedia = ({label, icon: Icon, href}: ButtonSocialProps) => { 
    return ( <a href={href} target="_blank" className="w-14 h-14 flex items-center justify-center border border-[#FFC480]/50 rounded-lg text-[#81DBC6] hover:text-white transition-colors duration-600 hover:bg-[#DB9881]/10">
        <Icon size={20}/>
    </a> );
}
 
export default ButtonSocialMedia;