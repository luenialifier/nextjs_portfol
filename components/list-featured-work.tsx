import CardFeaturedWork from "./ui/card-featured-work";

const ListFeaturedWork = () => {
    return ( 
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-10 mb-10">

                    <CardFeaturedWork href='/work/details' image="https://img.freepik.com/foto-gratis/mujer-buscando-monedas-criptograficas-usando-su-telefono-inteligente-mientras-casa_23-2149240357.jpg?w=740&t=st=1701451891~exp=1701452491~hmac=73d07ddc6ab33947ef70cf8c05402c596ce3e572886a8246f4cb2a7b890e13a4"
                        category="Bloofin" title="Diseño en diversos formatos" />

                    <CardFeaturedWork href='/work/details' image="https://img.freepik.com/foto-gratis/conjunto-stand-exposicion-comercial-realista-o-quiosco-exposicion-blanco-o-stand-corporativo_272375-4824.jpg?w=826&t=st=1701799455~exp=1701800055~hmac=6e910e33c5613e88b29d2bd3adfb175a61fe4139bd683ce606f97204d827abb9"
                        category="Bloofin" title="Diseño en diversos formatos" />

                    <CardFeaturedWork href='/work/details' image="https://img.freepik.com/foto-gratis/conjunto-stand-exposicion-comercial-realista-o-quiosco-exposicion-blanco-o-stand-corporativo_272375-4822.jpg?w=826&t=st=1701800063~exp=1701800663~hmac=aac195252445e6f7af6c54f559ae89e76bc2f15ebdceb6cfd6ce4694e427a904"
                        category="Bloofin" title="Diseño en diversos formatos" />

                </div>
     );
}
 
export default ListFeaturedWork;