import ButtonSocialMedia from '@/components/ui/button-social-media';
import Image from 'next/image'
import { FaInstagram , FaXTwitter, FaLinkedin , FaYoutube} from "react-icons/fa6";



/*export const socialMedia = [
    {
      socialMedia: 'Instagram',
      icon: FaInstagram,
      href: "https://instagram.com/chemomil",
    },
    {
      socialMedia: 'X',
      icon: FaXTwitter,
      href: "https://twitter.com/chemomaker",
    },
    {
      socialMedia: 'Linkedin',
      icon: FaLinkedin,
      href: "https://linkedin.com/chemomaker",
    },
    {
      socialMedia: 'Youtube',
      icon: FaYoutube,
      href: "https://youtube.com/chemomaker",
    }
  ];*/

  export const information = {
    about:
    'Soy un diseñador atrevido con 20 años de experiencia especializado en varios campos del diseño gráfico,impresión digital a gran formato -stands - web - nube',
    photo: 'https://img.freepik.com/foto-gratis/retrato-estudio-cigarrillos-fumadores-masculinos-rastafari-africanos-aislado-sobre-fondo-azul_613910-17499.jpg?t=st=1700669406~exp=1700670006~hmac=01297c4a4f5a92d478ea6eaba112e0edf98777a0291fa206a4d48c5809c310ba',
    socialMedia: [
        {
            socialMedia: 'Instagram',
            icon: FaInstagram,
            href: "https://instagram.com/chemomil",
          },
          {
            socialMedia: 'X',
            icon: FaXTwitter,
            href: "https://twitter.com/chemomaker",
          },
          {
            socialMedia: 'Linkedin',
            icon: FaLinkedin,
            href: "https://linkedin.com/chemomaker",
          },
          {
            socialMedia: 'Youtube',
            icon: FaYoutube,
            href: "https://youtube.com/chemomaker",
          }

    ],
  }

